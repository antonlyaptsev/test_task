(function() {
	'use strict';

	angular.module('testApp')
		   .factory('ExtrapageService', ExtrapageService);

	ExtrapageService.$inject = [];

	function ExtrapageService() {

		var currentOrganizations = [
				{
					name: 'Рога и Копыта',
					domain: 'www.example.com'
				},
				{
					name: 'Геркулес',
					domain: 'www.example2.com',
				}
			];


		return{
			
			getOrgs: function() {
				return currentOrganizations;
			},

			addOrg: function(name, domain) {
				var newOrg = {name: name, domain: domain};
				var currentOrganizations = this.getOrgs();
				currentOrganizations.push(newOrg);
			},

			deleteOrg: function(name) {
				var currentOrganizations = this.getOrgs();
				var i
				for (i = 0; i < currentOrganizations.length; ++i) {
					if (currentOrganizations[i].name == name) {
						currentOrganizations.splice(i, 1);
					}
				}
			}
		};
	}
})();