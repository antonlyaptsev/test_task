(function() {
    'use strict';

    angular
        .module('testApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('extrapage', {
            parent: 'app',
            url: '/extrapage',
            views: {
                'content@': {
                    templateUrl: 'app/js/extrapage/extrapage.html',
                    controller: 'ExtrapageController'
                }
            }
        });
    }
})();