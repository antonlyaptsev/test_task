(function() {
    'use strict';

    angular
        .module('testApp')
        .controller('ExtrapageController', ExtrapageController);

    ExtrapageController.$inject = ['$scope', 'ExtrapageService'];

    function ExtrapageController ($scope, ExtrapageService) {
        var ec = this;

		$scope.orgs = ExtrapageService.getOrgs();

		ec.saveOrg = function() {
			var name = $scope.org.name,
				domain = $scope.org.domain;
			ExtrapageService.addOrg(name, domain);
			$scope.orgs = ExtrapageService.getOrgs();
			console.log($scope.orgs);
		}

		ec.removeOrg = function(name) {
			ExtrapageService.deleteOrg(name);
			$scope.orgs = ExtrapageService.getOrgs();
		}

		ec.getUrl = function(domain) {
			var url = "http://" + domain;
			return url;
		}

    }
})();