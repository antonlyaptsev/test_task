(function() {
	'use strict';

	angular.module('testApp')
		   .factory('MainpageService', MainpageService);

	MainpageService.$inject = ['$resource'];

	function MainpageService($resource) {
		var resourceUrl = 'http://66.228.47.75\:4444/test/names';
		
		return $resource(resourceUrl, {}, {
			'post': {
				method: 'POST',
				isArray: false,
				headers: {
					"Access-Control-Allow-Origin" : "*",
            		"Access-Control-Allow-Methods" : "POST",
            		"Access-Control-Allow-Headers": "Content-Type, Access-Control-Allow-Headers",
            		"Accept": "application/json",
					"Content-Type": "application/json",
				},
				params: {
					email: 'somemail@somehost.com'
				},
				transformResponse: function(data) {
					data = angular.fromJson(data);
                    return data;
				}
			}
		});
	}
})();