(function() {
    'use strict';

    angular
        .module('testApp')
        .controller('MainpageController', MainpageController);

    MainpageController.$inject = ['$scope', 'MainpageService'];

    function MainpageController ($scope, MainpageService) {
        var mc = this;
        mc.orgs = [];

        var mock = {
            "data": [
                {
                    "domain":"test",
                    "name":"test org"
                },
                {
                    "domain":"kekchik",
                    "name":"another one"
                },
                {
                    "domain":"pif",
                    "name":"pfff"
                },
                {
                    "domain":"wewe",
                    "name":"wewe"
                },
                {
                    "domain":"test",
                    "name":"Test kest"
                },
                {
                    "domain":"ne",
                    "name":"ne"
                }
            ]
        }

        mc.loadAll = function() {
            /*mc.orgs = mock['data'];*/
        	MainpageService.post(function(result) {
        		mc.orgs = result['data'];
        	});

        }

        mc.getUrl = function(domain) {
			var url = "http://" + domain;
			return url;
		}

        mc.loadAll();



        
    }
})();