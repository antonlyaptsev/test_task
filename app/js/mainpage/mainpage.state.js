(function() {
    'use strict';

    angular
        .module('testApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('mainpage', {
            parent: 'app',
            url: '/',
            views: {
                'content@': {
                    templateUrl: 'app/js/mainpage/mainpage.html',
                    controller: 'MainpageController'
                }
            }
        });
    }
})();