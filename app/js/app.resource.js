(function() {
	'use strict';

	angular.module('testApp')
		   .config(resourceConfig);

	resourceConfig.$inject = ['$resourceProvider'];

	function resourceConfig($resourceProvider) {
		$resourceProvider.defaults.stripTrailingSlashes = false;
	}

})();